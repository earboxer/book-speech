# Author: Zach DeCook
# Created: May 13, 2018
if [ $# -eq 0 ]
then
  echo "Usage: ./speak.sh filename.txt [starting line number]"
  exit 0
fi

var=0

n=`cat $1 | wc -l`

# If a second parameter is passed, accept it is the line number  
if [ $# -eq 2 ]
then
  n=$((n-$2+1))
  var=$((var+$2-1))
fi

#Decide what synth to use
synth="none"
if test `uname` = "Darwin" && test `which say` != ""
then
  synth="say"
else if which termux-tts-speak
then
  synth="termux-tts-speak"
else if which espeak
then
  synth="espeak"
fi fi fi

cat $1 | tail -n $n | while read p
do
  var=$((var+1))
  case $synth in
    "espeak") echo $var "$p"; espeak "$p" -z;;
    "say") echo $var; say -i "$p" || sleep 1;;
    "termux-tts-speak") echo $var "$p"; termux-tts-speak -s MUSIC "$p";;
    *) echo "no engine could be selected"; exit 0;;
  esac
done

