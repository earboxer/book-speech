#stdin or file as arg
while(<>){
	if ( m/^\n$/ )
	{
		print "\n"
	}
	else
	{
		$_ =~ s/\n//;
		$_ =~ s/_+/_/g;
		$_ =~ s/([^0-9])\.\s+/\1\.\n/g;
		print $_;
	}
}
print "\n";
